package com.shicheng.media;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleRegistry;

import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.shicheng.mediaplayer.AudioSensorBinder;
import com.shicheng.mediaplayer.media.MediaCallBack;
import com.shicheng.mediaplayer.media.MediaManager;

public class MainActivity extends AppCompatActivity implements LifecycleOwner {
    //LifecycleRegistry 实现了Lifecycle
    private LifecycleRegistry mLifecycleRegistry=new LifecycleRegistry(MainActivity.this);

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AudioSensorBinder audioSensorBinder = new AudioSensorBinder(this);
        mLifecycleRegistry.addObserver(audioSensorBinder);
        findViewById(R.id.start_tv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaManager.builder()
                        .setContext(MainActivity.this)
                        .setDebugModel(true)
                        .setMediaListener(new MediaCallBack() {
                            @Override
                            public void progress(int i, int max) {

                            }

                            @Override
                            public void prepare(int duration, String time) {

                            }

                            @Override
                            public void start() {

                            }

                            @Override
                            public void end() {

                            }

                            @Override
                            public void stop() {

                            }
                        })
                        .setUrl("https://96.f.1ting.com/local_to_cube_202004121813/96kmp3/2020/09/21/21e_wc/01.mp3")
                        .build();
                MediaManager.start();
            }
        });
    }
}