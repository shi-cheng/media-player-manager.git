package com.shicheng.mediaplayer.media;

/**
 * Created by SHICHENG
 * <p>
 * Time on 2021/11/08
 */
public interface MediaCallBack {

    void progress(int i,int max);

    void prepare(int duration,String time);

    void start();

    void end();

    void stop();

}
