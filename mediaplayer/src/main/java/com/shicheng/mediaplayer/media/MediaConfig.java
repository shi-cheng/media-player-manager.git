package com.shicheng.mediaplayer.media;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by SHICHENG
 * <p>
 * Time on 2021/11/08
 */
public class MediaConfig implements Parcelable {


    public String url;
    public int maxProgress;
    public int currentProgress;
    public int currentStatus;
    public boolean isDebug = false;



    public MediaConfig() {
    }

    protected MediaConfig(Parcel in) {
        this.url = in.readString();
        this.maxProgress = in.readInt();
        this.currentProgress = in.readInt();
        this.currentStatus = in.readInt();
        this.isDebug = in.readByte() !=0;
    }

    public static final Creator<MediaConfig> CREATOR = new Creator<MediaConfig>() {
        @Override
        public MediaConfig createFromParcel(Parcel in) {
            return new MediaConfig(in);
        }

        @Override
        public MediaConfig[] newArray(int size) {
            return new MediaConfig[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(url);
        dest.writeInt(maxProgress);
        dest.writeInt(currentProgress);
        dest.writeInt(currentStatus);
        dest.writeByte(this.isDebug ? (byte)1 : (byte)0);
    }
}
